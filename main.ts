import i2c from "i2c-bus";
// const i2c = require('i2c-bus');
import { connect } from "async-mqtt";


const TEMP_ADDRESS = 0x48;
const TEMP_REG = 0x00;

const toCelsius = (rawData) => {
  const a = ((rawData & 0xff00) >>> 8) | ((rawData & 0xff) << 8);
  const b = a >>> 3;

  // 正負の部分を取得して判定
  if ((b & 0b1000000000000) !== 0) {
    console.warn("氷点下以下かもしれません", b * 0.0625);
  }
  return b * 0.0625;
};

const client = connect("mqtt://mqtt.beebotte.com", {
  port: 1883,
  username: process.env.MQTT_USERNAME,
  password: ""
});

// client.on("connect", ()=> {
//   client.subscribe("my_house/air_off");

//   client.on("message", (topic, message) => {
//     console.log("topic", topic)
//     console.log("message", message.toString());
//   });
// });

const sleep  = (ms: number) => {return new Promise( resolve => setTimeout(resolve, ms) )}

(async () => {
  
  const i2c1 = await i2c.openPromisified(1);

  while(true) {
    const word = await i2c1.readWord(TEMP_ADDRESS, TEMP_REG);

    const temperature = toCelsius(word)
    console.log("temperature", temperature)

    await client.publish("my_house/temperature", `${temperature}`)

    console.log("publish", temperature)

    await sleep(5000)
  }

})();
