[Unit]
Description=mqtt
After=network.target
StartLimitIntervalSec=30

[Service]
Type=simple
Restart=always
RestartSec=10
User=pi
ExecStart=yarn raspberrypi
StandardOutput=inherit
StandardError=journal
Environment="MQTT_USERNAME=token_9GqbPXIfN3THRfLj"
WorkingDirectory=/home/pi/mqtt-house-subscriber

[Install]
WantedBy=multi-user.target